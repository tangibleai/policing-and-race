import pandas as pd
pd.read_csv('https://data.austintexas.gov/api/views/sc6h-qr9f/rows.csv?accessType=DOWNLOAD')
df = _
df.head()
mask = df['RACE KNOWN'] == 'N'
mask[:10]
mask.sum()
len(df)
mask.sum() / len(df)
df.columns
df['RACE-ORIGIN CODE']
df['RACE-ORIGIN CODE'].iloc[0]
df['RACE-ORIGIN CODE'][0]
df['RACE-ORIGIN CODE'][mask]
df['RACE-ORIGIN CODE'][mask].value_counts()
df['RACE-ORIGIN CODE'][~mask].value_counts()
vc = df['RACE-ORIGIN CODE'][~mask].value_counts()
vc / vc.sum()
vc_unknown_race = df['RACE-ORIGIN CODE'][mask].value_counts()
vc_unknown_racec / vc_unknown_race.sum()
vc_unknown_race / vc_unknown_race.sum()
# P(W|U)
mask
race_unknown = mask
df['RACE-ORIGIN CODE'][mask] = pd.np.nan
pd.np
import numpy as np
race_origin_where_unknown = df['RACE-ORIGIN CODE'][race_unknown].copy()
len(race_origin_where_unknown)
len(df)
type(race_origin_where_unknown)
race_origin_where_unknown.index
df['race_origin_where_unknown'] = race_origin_where_unknown
df['race_origin_where_known'] = df['RACE-ORIGIN CODE'][~race_unknown].copy()
df.head()
df['race_origin_where_unknown'] = df['RACE-ORIGIN CODE'][race_unknown].copy()
df.head()
df.describe()
df.describe(include='all')
df_original = pd.read_csv('/Users/hobs/Dropbox/Docs/projects/teaching/internship/Racial_Profiling_Dataset_2015-_Citations.csv')
df_original.describe(include='all')
df_original['RACE KNOWN'].value_counts()
df_original['RACE KNOWN'].isnull()
df_original['RACE KNOWN'].isnull().sum()
df_original.head()
df = df_original
df.describe(include='all')
df['race_known_with_nans'] = df['RACE KNOWN'].copy()
df.describe(include='all')
df.race_known_with_nans.apply(lambda race_known: np.nan if race_known == 'N' else race_known)
df.race_known_with_nans.apply(lambda race_known: np.nan if race_known == 'N' else race_known).isnull().sum()
df['race_known_with_nans'] = df.race_known_with_nans.apply(lambda race_known: np.nan if race_known == 'N' else race_known)
df['race_known_with_nans'].apply(lambda race_known: np.nan if race_known == 'N' else race_known, inplace=True)
df['race_known_with_nans'] = df['race_known_with_nans'].apply(lambda race_known: np.nan if race_known == 'N' else 1)
df.describe()
import sklearn
from sklearn.linear_model import LinearRegression
df['race_known_with_nans_not_isna'] = ~['race_known_with_nans'].isna()
df['race_known_with_nans_not_isna'] = ~df['race_known_with_nans'].isna()
df.describe()
df['race_known_with_nans_not_isna']
df['race_known_with_nans_not_isna'].sum()
len(df)
df['race_known_with_nans_isna'] = df['race_known_with_nans'].isna()
len(df)
df.describe()
df.isna().sum()
df.isnull().sum()
LinearRegression
model = LinearRegression()
model
y = df['RACE-ORIGIN CODE']
X = df[['RACE KNOWN']]
X.head()
y.head()
df = pd.get_dummies(df)
df_cleaned = pd.get_dummies(df, columns=['RACE-ORIGIN CODE'])
df_cleaned
df_cleaned.head()
X = df_cleaned[[c for c in df_cleaned.columns if c.startswith('RACE-ORIGIN')]]
X.head()
y = df_cleaned['RACE KNOWN']
model.fit(X, y)
y = y == 'Y'
y[:10]
y.sum()
model.fit(X, y)
model.score(X, y)
y_pred = model.predict(X)
(y_pred == y).sum()
(y_pred == y.astype(int)).sum()
e = (y_pred - y.astype(int))
rmse = np.mean(e**2)**.5
rmse
model.coef_
pd.Series(model.coef_, X.columns)
from sklearn.linear_model import LogisticRegression
lr_model = model
model = LogisticRegression()
model
model.fit(X, y)
model.coef_
model.score(X, y)
y_pred_logistic = model.predict(X)
e = (y_pred - y.astype(int))
(y_pred_logistic == y).sum()
y_pred.sum()
y_pred_logistic_proba = model.predict_proba(X)
y_pred_logistic[:10]
y_pred_logistic[:100]
y_pred_logistic_proba[:100]
history
history -f /Users/hobs/Dropbox/Docs/projects/teaching/internship/racial_profiling.py
