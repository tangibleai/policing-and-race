# Policing And Race

Data Science models of policing actions in US cities, including arrests, traffic stops, and racial profiling stops.

Workshop videos available here: [Part 1](https://youtu.be/Kke2w_JDq60), [Part 2](https://youtu.be/-tpVXD4D3yA).